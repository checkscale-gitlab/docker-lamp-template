# Docker Template for LAMP installations

Example of using docker-compose to create a LAMP environment.

Makes two containers:

1. PHP/Apache. This is where the web app goes (HTML, PHP, etc.)
2. MariaDB. This is for the MySQL database.

## Customizations

You'll need to do a few things.

### PHP/Apache

Open [docker/php/Dockerfile](docker/php/Dockerfile).

1. Which version of PHP do you need? Fill out the image name.
2. Which php.ini do you want: production or development?
3. Which PHP extensions do you want?
4. Put your files and directories into COPY statements near the end.

### MariaDB

Open [docker/db/Dockerfile](docker/db/Dockerfile).

1. Copy your SQL dump or shell script into `/docker-entrypoint-initdb.d` to setup the DB

Open [docker-compose.yml](docker-compose.yml).

1. Customize `MYSQL_ROOT_PASSWORD` and `MYSQL_DATABASE` to what you need.

### Docker Compose

Open [docker-compose.yml](docker-compose.yml).

1. Change the image/service names as you see fit.

## Usage

	docker-compose up

Database connections, i.e. in PHP, can use the service host name (in this case `mydb`) and the specified password (in this case `pass123`).

## Examples

See @moralbacteria/bunny-fight
